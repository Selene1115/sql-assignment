import oracle.kv.KVStore;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;
import oracle.kv.Key;
import oracle.kv.Value;
import oracle.kv.ValueVersion;
import oracle.kv.KeyValueVersion;
import oracle.kv.Direction;

import java.util.ArrayList;
import java.util.Iterator;

class MatchSkills{

    public static void main(String args[]) {
        try {
		
			String storeName = "kvstore";
			String hostName = "data-pc17";
			String hostPort = "5000";
			String sneed[]={" "," "};
			
			ArrayList<String> majorComponents1 = new ArrayList<String>();
			ArrayList<String> majorComponents2 = new ArrayList<String>();
			
		    KVStore store;
						
			store = KVStoreFactory.getStore(new KVStoreConfig(storeName, hostName + ":" + hostPort));
            System.out.println("Store " + storeName + " at " + hostName + ":" + hostPort + " opened.");

			majorComponents1.add("Position");
			majorComponents1.add("00000001");
            Key key = Key.createKey(majorComponents1);
			Iterator<KeyValueVersion> it = store.storeIterator(Direction.UNORDERED, 0, key, null, null);
			while (it.hasNext() )
			{
				KeyValueVersion kvvi = it.next();	
				String skill=kvvi.getKey().toString();
				String array[]=skill.split("/");
				//System.out.println(array[4]+" "+array[5]);
				sneed[0]=array[4];
				sneed[1]=array[5];
			}

			majorComponents2.add("Applicant");
			majorComponents2.add(sneed[0]);
			majorComponents2.add(sneed[1]);
            key = Key.createKey(majorComponents2);
			it = store.storeIterator(Direction.UNORDERED, 0, key, null, null);
			while (it.hasNext() )
			{
				//System.out.println("!!!!");
				KeyValueVersion kvvi = it.next();	
				String skill=kvvi.getKey().toString();
				String array[]=skill.split("/");
				System.out.println("The applicanta have equal or higher skills: " + array[6]+" "+array[7]+" "+array[8]);
			}
			
            store.close();
			System.out.println("Store " + storeName + " at " + hostName + ":" + hostPort + " closed.");
			
        }
		catch (RuntimeException e) {
            e.printStackTrace();
        }
    }
}
