import oracle.kv.KVStore;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;
import oracle.kv.Key;
import oracle.kv.Value;
import oracle.kv.ValueVersion;
import java.util.ArrayList;

class LoadData{

    public static void main(String args[]) {
        try 
		{
		
			String storeName = "kvstore";
			String hostName = "data-pc17";
			String hostPort = "5000";
			
		    KVStore store;
			
			ArrayList<String> majorList = new ArrayList<String>();
		    ArrayList<String> minorList = new ArrayList<String>();
			Key key;
			Value value;
			String valueString; 
			
			store = KVStoreFactory.getStore(new KVStoreConfig("kvstore", "localhost" + ":" + "5000"));
            System.out.println("Store " + storeName + " at " + hostName + ":" + hostPort + " opened.");
			
			majorList.add("Position");
			majorList.add("00000001");
			majorList.add("SNeeded");
			majorList.add("JAVA-PROGRAMMING");
			majorList.add("COOKING");
			minorList.add("LECTURER");
			minorList.add("UNSW");
			minorList.add("45000.00");
			minorList.add("computer");
			valueString = "Teaching";
			key = Key.createKey(majorList, minorList);			
			value = Value.createValue(valueString.getBytes());
			store.put(key, value);
			majorList.clear();
			minorList.clear();
			
			majorList.add("Position");
			majorList.add("00000002");
			majorList.add("SNeeded");
			majorList.add("JAVA-PROGRAMMING");
			majorList.add("TRUCK DRIVING");
			minorList.add("LECTURER");
			minorList.add("UOW");
			minorList.add("450000.00");
			minorList.add("mouse pad");
			valueString = "Research";
			key = Key.createKey(majorList, minorList);			
			value = Value.createValue(valueString.getBytes());
			store.put(key, value);
			majorList.clear();
			minorList.clear();
			
			majorList.add("Applicant");
			majorList.add("JAVA-PROGRAMMING");
			majorList.add("COOKING");
			majorList.add("C PROGRAMMING");
			minorList.add("000001");
			minorList.add("PETER");
			minorList.add("JONES");
			minorList.add("7 STATION ST.");
			minorList.add("PERTH");
			minorList.add("WA");
			minorList.add("645278453");
			minorList.add("Applies");
			minorList.add("00000001");
			minorList.add("13-DEC-1999");
			minorList.add("00000006");
			minorList.add("26-OCT-1999");
			valueString = "Job expectations ? Money, money, money, ...";
			key = Key.createKey(majorList, minorList);			
			value = Value.createValue(valueString.getBytes());
			store.put(key, value);
			majorList.clear();
			minorList.clear();
			
			majorList.add("Applicant");
			majorList.add("JAVA-PROGRAMMING");
			majorList.add("TRUCK DRIVING");
			minorList.add("000002");
			minorList.add("JOHN");
			minorList.add("BLACK");
			minorList.add("23 VICTORIA ST.");
			minorList.add("GEELONG");
			minorList.add("Vic");
			minorList.add("63569784");
			minorList.add("63569785");
			minorList.add("blunder@hotmail.com");
			minorList.add("Applies");
			minorList.add("00000001");
			minorList.add("13-DEC-1999");
			minorList.add("00000006");
			minorList.add("27-OCT-1999");
			valueString = "Microsoft Access ? Oh yeah, kindergarten database system";
			key = Key.createKey(majorList, minorList);			
			value = Value.createValue(valueString.getBytes());
			store.put(key, value);
			majorList.clear();
			minorList.clear();
			
            store.close();
			System.out.println("Store " + storeName + " at " + hostName + ":" + hostPort + " closed.");
        }
		catch (RuntimeException e) {
            e.printStackTrace();
        }
    }
}
