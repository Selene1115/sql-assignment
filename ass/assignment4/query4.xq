for $a in /bookshop
return 
	<bookshop>
		{for $i in $a/product/@ID
		let $b := count($a/descendant[@IDREF=$i])
		return 
			<id> 
				{$i} 
				{$b} 
			</id>}
	</bookshop>