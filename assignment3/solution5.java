import java.sql.*;

class solution5
{
  public static void main (String args [])
       throws SQLException, ClassNotFoundException
  {
    // Load the Oracle JDBC driver
    Class.forName ("oracle.jdbc.driver.OracleDriver");
    Connection conn = DriverManager.getConnection
       ("jdbc:oracle:thin:@10.0.2.15:1521:ip100215",  "CSCI317", "csci317");
  try{
        System.out.println( "Connected to a database server: data-pc01 as a user: CSCI317" );
        System.out.println( );
        String query = "SELECT COUNT(*) FROM PARTSUPP " +
                       " JOIN SUPPLIER ON PS_SUPPKEY = S_SUPPKEY JOIN PART ON PS_PARTKEY = P_PARTKEY " +
                       " JOIN NATION   ON S_NATIONKEY = N_NATIONKEY " +
                       " WHERE N_NAME IN ('JAPAN', 'PERU', 'AUSTRALIA' ) AND S_SUPPKEY = ?";
	PreparedStatement stmt1 = conn.prepareStatement(query);
	//String runq;
	//Statement stmt1 = conn.createStatement ();
        int i;
        for ( i = 1; i < 500; i++)
        {
                //runq = query + i;
		stmt1.setInt(1, i);
	        ResultSet rset1 = stmt1.executeQuery();
		long totpartshipped= 0;
		while ( rset1.next() ) 
			totpartshipped = rset1.getInt(1);
			if ( totpartshipped != 0 )		
				System.out.println( "Supplier key: " + i + " Total parts shipped: " + 
                                                    totpartshipped );	
        }
        System.out.println( );
        System.out.println( "Have a nice day." );
    }
   catch (SQLException e )
   {
     String errmsg = e.getMessage();
     System.out.println( errmsg );
   }
  }
}
