import java.sql.*;
class solution8
{
  public static void main (String args [])
       throws SQLException, ClassNotFoundException
  {
    Class.forName ("oracle.jdbc.driver.OracleDriver");
    Connection conn = DriverManager.getConnection
       ("jdbc:oracle:thin:@10.0.2.15:1521:ip100215",  "csci317", "csci317");
  try{
	System.out.println( "Just started");
	Statement stmt = conn.createStatement();
        ResultSet rset = stmt.executeQuery( "SELECT COUNT(*) FROM ORDERS WHERE O_TOTALPRICE > 200000" );
	int counter =0;
        float o_totalprice;
	while ( rset.next() ) 
        {
		counter = rset.getInt(1);
	}
	System.out.println( "Total number of orders above 200K = " + counter );
    }
   catch (SQLException e )
   {
     String errmsg = e.getMessage();
     System.out.println( errmsg );
   }
  }
}


/* In task8.java, there is 54999443 bytes data transmitted by the application over a network, 
   but in solution8.java, there is 544 bytes data transmitted by the application over a network.
   The SQL query in task8 selects all the rows from ORDERS table, and then count the number, so
   too many data are transmitted over a network which increases the processing time. The processing 
   time of task8 is 29.035s, the processing time of solution8 is 2.450s. */
   
